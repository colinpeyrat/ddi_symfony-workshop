<?php

namespace AppBundle\DataFixtures\ORM;

use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Entity\Plant;

class LoadPlantData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $plant = new Plant();
        $plant->setLat(45.8974472);
        $plant->setLng(6.1255241);
        $plant->setIsWatered(false);
        $plant->setPicture("http://placehold.it/50x50");
        $plant->setLastWatering(new DateTime());
        $plant->setNextWatering(new DateTime());
        $plant->setGardener($this->getReference('gardener'));


        $manager->persist($plant);

        $plant2 = new Plant();
        $plant2->setLat(45.8274472);
        $plant2->setLng(6.1455241);
        $plant2->setIsWatered(false);
        $plant2->setPicture("http://placehold.it/50x50");
        $plant2->setLastWatering(new DateTime());
        $plant2->setNextWatering(new DateTime());
        $plant2->setGardener($this->getReference('gardener'));

        $manager->persist($plant2);
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}