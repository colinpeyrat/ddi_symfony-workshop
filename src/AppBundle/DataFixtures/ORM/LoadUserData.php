<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\SuperAdmin;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Plant;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $superAdmin = new User();
        $superAdmin->setUsername('superjardinier');
        $superAdmin->setEmail('superjardinier@jardinerie.com');
        $superAdmin->setPlainPassword('jardinerie');
        $superAdmin->setFirstName('Robert');
        $superAdmin->setLastName('Delafondue');
        $superAdmin->setPathPicture('');
        $superAdmin->addRole('ROLE_ADMIN');
        $superAdmin->setEnabled(true);

        $userManager->updateUser($superAdmin, true);

        $gardener = new User();
        $gardener->setUsername('jardinier');
        $gardener->setEmail('jardinier@jardinerie.com');
        $gardener->setPlainPassword('jardinerie');
        $gardener->setFirstName('Michel');
        $gardener->setLastName('Delaraclette');
        $gardener->setPathPicture('');
        $gardener->addRole('ROLE_USER');
        $gardener->setEnabled(true);

        $userManager->updateUser($gardener, true);


        $this->addReference('gardener', $gardener);


    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}