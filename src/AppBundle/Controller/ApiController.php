<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @Route("/api/plants", name="api_plants")
     */
    public function plantsAction(Request $req)
    {
        if ($req->isXmlHttpRequest()) {
            $repository = $this->getDoctrine()->getRepository('AppBundle:Plant');
            $response = array();
            $plants = $repository->findAll();

            // On filtre pour éviter de retourner
            // les infos sensibles (id, etc.)
            foreach ($plants as $plant) {
                $response[] = array(
                    'lat' => $plant->getLat(),
                    'lng' => $plant->getLng(),
                    'isWatered' => $plant->getIsWatered(),
                    'nextWatering' => $plant->getNextWatering()->format('d/m/Y H:i:s'),
                    'lastWatering' => $plant->getLastWatering()->format('d/m/Y H:i:s')
                );
            }

            return new JsonResponse(json_encode($response));
        }
    }
}
