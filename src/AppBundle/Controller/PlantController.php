<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Plant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Plant controller.
 *
 * @Route("plant")
 */
class PlantController extends Controller
{
    /**
     * Lists all plant entities.
     *
     * @Route("/", name="plant_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $plants = $em->getRepository('AppBundle:Plant')->findAll();

        return $this->render('plant/index.html.twig', array(
            'plants' => $plants,
        ));
    }

    /**
     * Creates a new plant entity.
     *
     * @Route("/new", name="plant_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        // Définis le jardinier de la plante comme étant le jardinier actuel
        $gardener = $this->getUser();

        $plant = new Plant();
        $form = $this->createForm('AppBundle\Form\PlantType', $plant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $plant->setGardener($gardener);
            $em->persist($plant);
            $em->flush($plant);

            return $this->redirectToRoute('plant_show', array('id' => $plant->getId()));
        }

        return $this->render('plant/new.html.twig', array(
            'plant' => $plant,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a plant entity.
     *
     * @Route("/{id}", name="plant_show")
     * @Method("GET")
     */
    public function showAction(Plant $plant)
    {
        $deleteForm = $this->createDeleteForm($plant);

        return $this->render('plant/show.html.twig', array(
            'plant' => $plant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing plant entity.
     *
     * @Route("/{id}/edit", name="plant_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Plant $plant)
    {
        $deleteForm = $this->createDeleteForm($plant);
        $editForm = $this->createForm('AppBundle\Form\PlantType', $plant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('plant_edit', array('id' => $plant->getId()));
        }

        return $this->render('plant/edit.html.twig', array(
            'plant' => $plant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a plant entity.
     *
     * @Route("/{id}", name="plant_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Plant $plant)
    {
        $form = $this->createDeleteForm($plant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($plant);
            $em->flush($plant);
        }

        return $this->redirectToRoute('plant_index');
    }

    /**
     * Creates a form to delete a plant entity.
     *
     * @param Plant $plant The plant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Plant $plant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plant_delete', array('id' => $plant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
