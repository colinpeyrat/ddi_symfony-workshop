<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="plant")
 */
class Plant
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @ORM\Column(type="float")
     */
    private $lng;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="plants")
     * @ORM\JoinColumn(name="gardener_id", referencedColumnName="id")
     */
    private $gardener;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $picture;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWatered;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastWatering;

    /**
     * @ORM\Column(type="datetime")
     */
    private $nextWatering;

    function __toString()
    {
        return $this->id;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Plant
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Plant
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float 
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return Plant
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set isWatered
     *
     * @param boolean $isWatered
     * @return Plant
     */
    public function setIsWatered($isWatered)
    {
        $this->isWatered = $isWatered;

        return $this;
    }

    /**
     * Get isWatered
     *
     * @return boolean 
     */
    public function getIsWatered()
    {
        return $this->isWatered;
    }

    /**
     * Set lastWatering
     *
     * @param \DateTime $lastWatering
     * @return Plant
     */
    public function setLastWatering($lastWatering)
    {
        $this->lastWatering = $lastWatering;

        return $this;
    }

    /**
     * Get lastWatering
     *
     * @return \DateTime 
     */
    public function getLastWatering()
    {
        return $this->lastWatering;
    }

    /**
     * Set nextWatering
     *
     * @param \DateTime $nextWatering
     * @return Plant
     */
    public function setNextWatering($nextWatering)
    {
        $this->nextWatering = $nextWatering;

        return $this;
    }

    /**
     * Get nextWatering
     *
     * @return \DateTime 
     */
    public function getNextWatering()
    {
        return $this->nextWatering;
    }

    /**
     * Set gardener
     *
     * @param \AppBundle\Entity\User $gardener
     *
     * @return Plant
     */
    public function setGardener(\AppBundle\Entity\User $gardener = null)
    {
        $this->gardener = $gardener;

        return $this;
    }

    /**
     * Get gardener
     *
     * @return \AppBundle\Entity\User
     */
    public function getGardener()
    {
        return $this->gardener;
    }
}
