Jardinerie
================

Tout d'abord lancer les commandes :

```bash
composer install
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
```

Ensuite pour insérer les données de test en base :

```bash
php app/console doctrine:fixtures:load
```

Pour accéder au back-office en tant que :

**Super Jardinier** (login: superjardinier, mdp: jardinerie)

**Jardinier** (login: jardinier, mdp: jardinerie)