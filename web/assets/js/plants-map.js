function addMarkers(data) {

    var plants = JSON.parse(data);

    var center = {lat: plants[0].lat, lng: plants[0].lng};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: center
    });

    var plant;

    var loop = function loop() {
        plant = plants[i];

        var title = (plant.isWatered) ? 'A été arrosée' : 'Non arrosée';
        var content = (plant.isWatered) ? 'Prochain arrosage le : <br><b>' + plant.nextWatering + '</b>': 'Dernier arrosage le : <br><b>' + plant.lastWatering + '</b>';

        contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h3 id="firstHeading" class="firstHeading">' + title + ' </h3>'+
            '<div id="bodyContent">'+
            '<p>' + content +  '<p>'+
            '</div>'+
            '</div>';


        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        var marker = new google.maps.Marker({
            position: {lat: plant.lat, lng: plant.lng},
            map: map,
            title: 'Hello World!'
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    };

    for (var i = 0; i < plants.length; i++) {
        var contentString;

        loop();
    }

}

function initMap() {
    var url = routeApi || "/api/plants";

    $.ajax({
        url: url,
        method: "get"
    }).done(addMarkers);
}